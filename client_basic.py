#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

    if len(sys.argv) < 4:
        print("Usage: python3 client_basic.py <server_ip> <server_port> <message>")
        exit()
    else:

        server_ip = sys.argv[1]
        server_port = int(sys.argv[2])
        message = sys.argv[3:]
        message = " ".join(message)

        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.sendto(message.encode('utf-8'), (server_ip, server_port))
                data = my_socket.recv(1024)
                print(data.decode('utf-8'))

            print("Cliente terminado.")
        except ConnectionRefusedError:
            print("Error conectando a servidor")


if __name__ == "__main__":
    main()
