#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    """M´etodo principal"""
    if len(sys.argv) < 5:
        print("Usage: client_sip.py <ip> <puerto> register <sip_address> <expires_value>")
        exit()
    else:

        server_ip = sys.argv[1]
        server_port = int(sys.argv[2])
        expires_value = int(sys.argv[5])
        message = ''

        if sys.argv[3] == 'register':
            user = sys.argv[4]
            message = ('REGISTER ' + 'sip:' + user + ' SIP/2.0\r\n'+'Expires: ' + str(expires_value) + '\r\n\r\n')
        else:
            print("Usage: client_sip.py <ip> <puerto> register <sip_address> <expires_value>")
            exit()

        try:

            # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.sendto(message.encode('utf-8'), (server_ip, server_port))
                data = my_socket.recv(1024)
                print(data.decode('utf-8'))

            print("Cliente terminado.")
        except ConnectionRefusedError:
            print("Error conectando a servidor")


if __name__ == "__main__":
    main()
