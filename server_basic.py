#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        received = data.decode('utf-8')
        print(f"{client_ip} {client_port} {received}")
        r = received.upper()
        sock.sendto(r.encode('utf-8'), self.client_address)



def main():

    if len(sys.argv) != 2:
        print("Usage: python3 server_basic.py <port>")
        exit()
    else:
        port = int(sys.argv[1])
        try:
            serv = socketserver.UDPServer(('', port), EchoHandler)
            print(f"Server listening in port {port}")
        except OSError as e:
            sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

        try:
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Finalizado servidor")
            sys.exit(0)


if __name__ == "__main__":
    main()
