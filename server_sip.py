#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa servidor que funciona como Registrar SIP
"""

import socketserver
import sys
import json
import time


class SIPRequest:
    """Clase que analiza las peticiones"""

    def __init__(self, data):
        """M´etodo inicializador de la clase"""
        self.data = data

    def parse(self):
        """M´etodo que analiza e inicializa instancias del objeto"""
        line = self.data.decode('utf-8')
        line = line.split('\r\n')
        first_nl = line[1]
        line = line[0]
        line = ''.join(line).split()
        self._parse_command(line)
        self._parse_headers(first_nl)

    def _get_address(self, uri):
        """ Extrae la direcci´on en una dupla desde la URI"""
        uri = uri.split(':')
        address = uri[1]
        schema = uri[0]
        return address, schema

    def _parse_command(self, line):
        """Recibe la primera linea de la petici´on e inicializa variables de instancia"""
        self.command = line[0]
        self.uri = line[1]
        self.address = self._get_address(self.uri)[0]

        if self._get_address(self.uri)[1] != 'sip':
            self.result = '416 Unsupported URI Scheme'

        elif self.command != 'REGISTER':
            self.result = '405 Method Not Allowed'

        else:
            self.result = '200 OK'

    def _parse_headers(self, first_nl):
        """Devuelve la variable de instancia con las cabeceras"""
        if first_nl == '':
            self.headers = ' '
        else:
            nombre_cabecera = first_nl.split(':')[0]
            valor_cabecera = first_nl.split(':')[1]
            self.headers = {nombre_cabecera: valor_cabecera}
        return self.headers


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    """Clase manejadora"""
    dict_users = {}
    list_users = []

    def process_register(self, obj):
        """Maneja el diccionario de usuarios tras recibir un REGISTER"""
        self.dict_users[obj.address] = self.client_address[0]
        if int(obj.headers['Expires']) == 0:
            del self.dict_users[obj.address]

    def registered2json(self, obj):
        """Devuelve un fichero json con los datos de los usuarios"""
        expires_value = float(obj.headers['Expires']) + time.time()
        expires_value = time.strftime(
            '%Y-%m-%d %H:%M:%S +0000',
            time.localtime(expires_value))

        self.list_users.append(obj.address)
        self.list_users.append({'address': self.client_address[0], 'expires': expires_value})
        with open('registered.json', "w") as json_file:
            json.dump(self.list_users, json_file, indent=2)

    @classmethod
    def json2registered(cls):
        """Comprueba si ya existe un fichero json, si lo hay, extrae de ´el los datos de los usuarios"""
        try:
            with open('registered.json', 'r') as json_file:
                cls.list_users = json.load(json_file)
        except FileNotFoundError:
            pass

    def handle(self):
        """M´etodo manejador de la clase"""
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
            self.registered2json(sip_request)

        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    """M´etodo principal"""
    if len(sys.argv) != 2:
        print("Usage: python3 server_basic.py <port>")
        exit()
    else:
        port = int(sys.argv[1])
        try:
            SIPRegisterHandler.json2registered()
            serv = socketserver.UDPServer(('', port), SIPRegisterHandler)
            print(f"Server listening in port {port}")
        except OSError as e:
            sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

        try:
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Finalizado servidor")
            sys.exit(0)


if __name__ == "__main__":
    main()
